语法没有强制性的规则，但是作为程序员应该有清晰的思路和优雅的写法，程序也不应该是一个人的东西，所以为了他们欣赏，我们也应该书写出利于阅读的代码。这样，规范就应运而生了。推荐阅读google, github的代码风格。地址如下：

- [https://code.google.com/p/google-styleguide/](https://code.google.com/p/google-styleguide/)
- [https://github.com/styleguide](https://github.com/styleguide)
## 检测工具
### HTML
使用在线检测工具， [W3验证工具](http://validator.w3.org/)

### CSS

### JavaScript
JSHint