# css模块化命名
任何模块在页面上都像是一个盒子一样，不和页面上其他元素相互影响。

## 模块命名
模块依据功能划分，比如模块tab， 命名为`ui-tab`。

子模块元素为`ui-tab-trigger`或者`ui-tab-container`。

子模块下属组件可以为`ui-tab-trigger-cell`。

子模块的状态为`ui-tab-trigger-cell-current`, `ui-tab-trigger-cell-active`等。

实例为:
```
<div class="ui-box">
   <h3 class="ui-box-title"></h3>
   <p class="ui-box-conent"></p>
</div>
```
所有状态都加在外层：
```
<div class="ui-box ui-box-hover">
   <h3 class="ui-box-title"></h3>    
   <p class="ui-box-content"></p>
</div>
```


## 常用模块
cnt(content)，hd(header)，text(txt)，img(images/pic)，title，item，cell 等， 只要词义表达了组件要实现的功能或者要表现出来的的外观就可以了。

## 常用状态
hover, current, selected, disabled, focus, blur, checked, success, error 等。通常你的命名应该看起来像 .ui-name-hover, .ui-name-error 这样。


## 通用模块前缀
`ui-`， `fn-`等。